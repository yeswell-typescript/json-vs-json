import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'iterable' })
export class IterablePipe implements PipeTransform
{
  public transform (value: { [key: string]: any }, args?: any): Array<{ key: string, value: any }> {
    return Object.keys(value)
      .reduce((result: any, key) => {
        result.push({ key, value: value[key] });

        return result;
      }, []);
  }
}