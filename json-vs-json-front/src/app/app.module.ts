import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { JsonChangingComponent } from './json-changing/json-changing.component';
import { IterablePipe } from './iterable.pipe';

@NgModule({
  declarations: [
    AppComponent,
    JsonChangingComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [IterablePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
