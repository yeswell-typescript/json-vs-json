import { Component, OnInit } from '@angular/core';
// import test from '../jsons/test.json';

export type DiffObject <T> = {
  $isDiff: true;
  oldValue?: T | null;
  newValue?: T | null;
} | {
  $isDiff: false;
  value: T | null;
};

@Component({
  selector: 'app-json-changing',
  templateUrl: './json-changing.component.html',
  styleUrls: ['./json-changing.component.scss']
})

export class JsonChangingComponent implements OnInit
{
  public testJson: Record<string, DiffObject<unknown>> = {
    snake: { '$isDiff': false, value: true },
    central: { '$isDiff': true, oldValue: 'though', newValue: '' },
    ball: { '$isDiff': true, oldValue: 'lead', newValue: 'stone' },
    though: { '$isDiff': false, value: 172 },
    blank: { '$isDiff': true, newValue: 'larger' }
  }

  public jsonToArray = Object.entries(this.testJson)
  
  ngOnInit(): void {

  }

  public checkDiff(item: DiffObject<unknown>): boolean {
    if (!item.$isDiff)
    {
      return false
    }
    return ('oldValue' in item) && ('newValue' in item)
  }
}
