import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { JsonChangingComponent } from './json-changing.component';
import { IterablePipe } from '../iterable.pipe';

@NgModule({
  declarations: [
    //pipes
    IterablePipe
  ],
  imports: [],
  providers: [JsonChangingComponent, IterablePipe],
  bootstrap: []
})
export class AppModule { }
